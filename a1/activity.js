const express = require('express');
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.get('/home', (request, response) => {
	response.send('Welcome to the home page');
});

let users = [];

app.post('/signup', (request, response) => {
	if (request.body.username !== '' && request.body.password !== '') {
		users.push(request.body);
		response.send('Sign up successfully');
	} else {
		response.send('Please input BOTH username and password');
	}
});

app.get('/users', (request, response) => {

	for (let user of users) {
		response.send(user);
	}
});

app.delete('/delete-user', (request, response) => {

	// If there is no input from Postman
	if (request.body.username === undefined) {
		let name;
		
		for (let ctr = 0; ctr < users.length; ctr++) {
			name = users[ctr].username;
		}

		if (users.length > 1) {
			users.pop();
		}

		response.send(`User ${name} has been deleted!`);

	// If there is a specified username to delete
	} else {
		let result;
		let index;

		for (let user of users) {
			if (request.body.username === user.username) {
				index = users.indexOf(request.body.username);
				users.splice(index, 1);
				result = `User ${request.body.username} has been deleted`;	
			} else {
				result = 'User does not exist';
			}
		} 
		response.send(result);
	}
	
});


app.listen(port, () => console.log(`Server running at port ${port}`));
