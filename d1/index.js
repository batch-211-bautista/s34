/*
	- Use the "require" directive to load the express module/package
	- A "module" is a software component or part of a program that contains one or more routines
	- This is used to get the contents of the package to be used by our application
	- It also allows us to access methods and functions that will allow us to easily create a server
*/
const express = require('express');

/*
	- Create an application using express
	- This creates an express application and stores this in a constant called app
	- In layman's term, app is our server
*/
const app = express();

// For our application server to run, we need a port to listen to
const port = 3000;

// MIDDLEWARES

/*
	- Set up for allowing the server to handle data from requests
	- Allows your application to read json data
	- Methods used from express.js are middlewares
	- Middleware is a layer of software that enables interaction and transmission of information between assorted applications.	
*/
app.use(express.json());
/*
	- Allows your app to read data from forms
	- By default, information received from the URL can only be received as a string or an array
	- By applying the option of "extended: true", we are allowed to receive information in  other data types such as an object throughout our application.
*/
app.use(express.urlencoded({extended: true}));

// ROUTES
/*
	- Express has methods corresponding to each HTTP method
	- The full base URI for our local application for this route will be at "http://localhost:3000"
*/

// RETURN SIMPLE MESSAGE
/*
	- This route expects to receive a GET  request at the base URI "/"

	POSTMAN:
	url: http://localhost:3000/
	method: GET
*/
app.get('/', (request, response) => {
	response.send('Hello World');
})

// RETURNS SIMPLE MESSAGE
/*
	URI: /hello
	method: GET

	POSTMAN
	url: http://localhost:3000/hello
	method: GET
*/
app.get('/hello', (request, response) => {
	response.send('Hello from the "/hello" endpoint');
})

// RETURNS SIMPLE GREETING
/*
	URI: /hello
	method: 'POST'

	POSTMAN
	url: http://localhost:3000/hello
	method: POST
	body: raw + json
		{
			"firstName": "Edmar",
			"lastName": "Bautista"
		}
*/

app.post('/hello', (request, response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}! This is from the "/hello" endpoint but with a post method`);
});

let users = [];

// REGISTER USER ROUTE
/*
	- This route expects to receive a POST request at the URI "/register"
	- This will create a user object in the "users" variable that mirrors a real world registration process

	URI: /hello
	Method: 'POST'

	POSTMAN:
	url: http://localhost:3000/register
	method: POST
	body: raw + json
		{
			"username": "Edmar",
			"password": " "
		}
*/

app.post('/register', (request, response) => {

	if (request.body.username !== '' && request.body.password !== '') {
		users.push(request.body);
		response.send(`User ${request.body.username} successfully registered!`);
		console.log(request.body);
	} else {
		response.send(`Please input BOTH username and password`);
	}

});

// CHANGE PASSWORD ROUTE
/*
	- This route expects to receive a PUT request at the URI "/change-password"
	- This will update the password of a user that matches the information provided in the client/postman

	URI: /change-password
	method: 'PUT'
	
	POSTMAN:
	url: http://localhost:3000/change-password
	method: PUT
	body: raw + json
		{
			"username": "Edmar",
			"password": "EdmarEdmar"
		}

*/
app.use('/change-password', (request, response) => {
	// Creates a variable to store the message to be sent bak to the client/Postman
	let message;
	console.log('Works after the message');

	// Creates a for loop that will loop through the elements of the "users" array
	for (let i = 0; i < users.length; i++) {
		// If the username provided in the client/Postman and the username of the current object in the loop is the same
		console.log('Works before the if');
		if (request.body.username == users[i].username) {
			// Changes the password of the user found by the loop into the password provided in the client/Postman
			users[i].password = request.body.password;
			// Changes the message to be sent if the password has been updated
			message = `User ${request.body.username}'s password has been updated`
			// Breaks out of the loop once a user that matches the username provided in the client/Postman is found
			break;
		// If no user is found, else
		} else {
			// Changes the message to be sent back by the response
			message = "User does not exist";
		}
	}

	console.log('Works before the response send');
	// Sends a response back to the client/Postman once the password has been updated or if a user has not been found
	response.send(message);
})


/*
	- Tells our server to listen to the port
	- If the port is accessed, we can run the server
	- Returns a message to confirm that the server is running in the terminal
*/
app.listen(port, () => console.log(`Server running at port ${port}`));